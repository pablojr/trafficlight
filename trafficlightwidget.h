#ifndef TRAFFICLIGHTWIDGET_H
#define TRAFFICLIGHTWIDGET_H

#include <QWidget>
#include <QState>
#include "lightwidget.h"

class TrafficLightWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TrafficLightWidget(QWidget *parent = nullptr);
    LightWidget *redLight() const;
    LightWidget *yellowLight() const;
    LightWidget *greenLight() const;
    QState *createLightState(LightWidget *light, int duration, QState *parent = nullptr);

private:
    LightWidget *m_red;
    LightWidget *m_yellow;
    LightWidget *m_green;
};

#endif // TRAFFICLIGHTWIDGET_H
