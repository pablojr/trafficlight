#include "trafficlightwidget.h"
#include <QVBoxLayout>
#include <QPalette>
#include <QTimer>
#include <QState>
#include <QFinalState>

TrafficLightWidget::TrafficLightWidget(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *vbox = new QVBoxLayout(this);
    m_red = new LightWidget(Qt::red);
    vbox->addWidget(m_red);
    m_yellow = new LightWidget(Qt::yellow);
    vbox->addWidget(m_yellow);
    m_green = new LightWidget(Qt::green);
    vbox->addWidget(m_green);
    QPalette pal = palette();
    pal.setColor(QPalette::Background, Qt::black);
    setPalette(pal);
    setAutoFillBackground(true);
}

LightWidget *TrafficLightWidget::redLight() const
{
    return m_red;
}

LightWidget *TrafficLightWidget::yellowLight() const
{
    return m_yellow;
}

LightWidget *TrafficLightWidget::greenLight() const
{
    return m_green;
}

QState *TrafficLightWidget::createLightState(LightWidget *light, int duration, QState *parent)
{
    QState *lightState = new QState(parent);
    QTimer *timer = new QTimer(lightState);
    timer->setInterval(duration);
    timer->setSingleShot(true);
    QState *timing = new QState(lightState);
    connect(timing, SIGNAL(entered()), light, SLOT(turnOn()));
    connect(timing, SIGNAL(entered()), timer, SLOT(start()));
    connect(timing, SIGNAL(exited()), light, SLOT(turnOff()));
    QFinalState *done = new QFinalState(lightState);
    timing->addTransition(timer, SIGNAL(timeout()), done);
    lightState->setInitialState(timing);
    return lightState;
}
