#ifndef LIGHTWIDGET_H
#define LIGHTWIDGET_H

#include <QWidget>
#include <QPaintEvent>
#include <QColor>

class LightWidget : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(bool on READ isOn WRITE setOn)

public:
    explicit LightWidget(const QColor &color, QWidget *parent = nullptr);
    bool isOn() const;
    void setOn(bool on);

signals:

public slots:
    void turnOff();
    void turnOn();

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    QColor m_color;
    bool m_on;
};

#endif // LIGHTWIDGET_H
