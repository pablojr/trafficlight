#include "trafficlight.h"
#include <QStateMachine>
#include <QState>
#include <QVBoxLayout>
#include "trafficlightwidget.h"

TrafficLight::TrafficLight(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *vbox = new QVBoxLayout(this);
    TrafficLightWidget *widget = new TrafficLightWidget();
    vbox->addWidget(widget);
    vbox->setMargin(0);

    QStateMachine *machine = new QStateMachine(this);
    QState *redGoingYellow = widget->createLightState(widget->redLight(), 3000);
    redGoingYellow->setObjectName("redGoingYellow");
    QState *yellowGoingGreen = widget->createLightState(widget->yellowLight(), 1000);
    yellowGoingGreen->setObjectName("yellowGoingGreen");
    redGoingYellow->addTransition(redGoingYellow, SIGNAL(finished()), yellowGoingGreen);
    QState *greenGoingYellow = widget->createLightState(widget->greenLight(), 3000);
    greenGoingYellow->setObjectName("greenGoingYellow");
    yellowGoingGreen->addTransition(yellowGoingGreen, SIGNAL(finished()), greenGoingYellow);
    QState *yellowGoingRed = widget->createLightState(widget->yellowLight(), 1000);
    yellowGoingRed->setObjectName("yellowGoingRed");
    greenGoingYellow->addTransition(greenGoingYellow, SIGNAL(finished()), yellowGoingRed);
    yellowGoingRed->addTransition(yellowGoingRed, SIGNAL(finished()), redGoingYellow);

    machine->addState(redGoingYellow);
    machine->addState(yellowGoingGreen);
    machine->addState(greenGoingYellow);
    machine->addState(yellowGoingRed);
    machine->setInitialState(redGoingYellow);
    machine->start();
}
