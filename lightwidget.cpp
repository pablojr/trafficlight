#include "lightwidget.h"
#include <QPainter>

LightWidget::LightWidget(const QColor &color, QWidget *parent)
     : QWidget(parent), m_color(color), m_on(false)
{

}

void LightWidget::turnOff()
{
    setOn(false);
}

void LightWidget::turnOn()
{
    setOn(true);
}

bool LightWidget::isOn() const
{
    return m_on;
}

void LightWidget::setOn(bool on)
{
    if (on != m_on) {
        m_on = on;
        update();
    }
}

void LightWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    if (m_on) {
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.setBrush(m_color);
        painter.drawEllipse(0, 0, width(), height());
    }
}
